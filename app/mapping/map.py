import sys

from multiprocessing import Pool

import app.mapping.util as utils


def map_local_network():

    # Create a base IP, based on first 3 octets of this device's ip. Like 192.168.1.xxx
    ip_base = '.'.join(utils.my_ip().split('.')[:3])+'.'

    # Run parallel processes to map ips 0-251 by creating a pool of threads
    with Pool(processes=26) as pool:
        ips = []
        for octet in range(0, 256):
            ips.append(ip_base+str(octet))

        data = pool.map(utils.ping, ips)
        pool.close()

    good_ips = []
    for item in data:
        if item is not None:
            good_ips.append(item)

    return good_ips
