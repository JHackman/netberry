# Utility methods for main script

import os
import socket
import subprocess


def my_ip():
    """
    Find my IP address
    :return: my IP, str
    """
    # Open the socket with our router host
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    # Connect with default subnet mask
    s.connect(("8.8.8.8", 80))
    ip = s.getsockname()[0]
    s.close()
    return ip


def ping(ip_target):
    """
    Do Ping
    :param ip_target: The ip to ping
    :return: True if successful, else false
    """
    try:
        # Make the process call, suppressing its output to devnull
        subprocess.check_call(['ping', '-c1', ip_target], stdout=open(os.devnull, 'wb'))
        return ip_target

    # Connection error signifies that we could not ping this IP, so move to the next one
    except:
        return None
