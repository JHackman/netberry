with open('log.txt', 'r') as logfile:
    print('Last run time: {}'.format(logfile.readline().rstrip()))

    devices = []
    for line in logfile:
        devices.append(line.rstrip())

    print('Devices connected on last run ({}):'.format(len(devices)))
    print(devices)
