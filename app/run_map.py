from datetime import datetime, timedelta
from multiprocessing import Pool

from app.mapping.map import map_local_network
from app.mapping.util import ping
from app.mapping.util import my_ip


def main():
    starttime = datetime.now()
    print("Running nmap. Estimated completion time: {}.".format(starttime+timedelta(minutes=2)))
    nmap = map_local_network()
    endtime = datetime.now()

    print('There are {} devices on the network.'.format(len(nmap)))
    print('These devices are located at the following addresses (your IP: {}):'.format(my_ip()))
    print(nmap)
    print()
    print('nmap took {}'.format(endtime-starttime))

    # Read/write the map to the logfile
    with open('log.txt', 'r') as logfile:
        # Collect devices on last run
        new_devices = []
        lost_devices = []
        old_devices = []
        last_run_date = logfile.readline()

        # Create list of devices used in the last run
        for device in logfile:
            old_devices.append(device.rstrip())

        # Create a list of devices that were in the previous run but not the newest run
        for device in old_devices:
            if device not in nmap:
                lost_devices.append(device)

        # Create a list of devices in the newest run but weren't in the previous run
        for device in nmap:
            if device not in old_devices:
                new_devices.append(device)

    with open('log.txt', 'w') as logfile:
        # Write this run's data to the log
        logfile.seek(0)
        logfile.truncate()
        logfile.write(str(endtime)+'\n')
        for device in nmap:
            logfile.write(device+'\n')

    print('Last run time: {}'.format(last_run_date))
    print('New devices since last run ({}):'.format(len(new_devices)))
    print(new_devices)
    print('Devices disconnected since last run ({}):'.format(len(lost_devices)))
    print(lost_devices)
    print('The results of this run will be recorded to ./log.txt')
