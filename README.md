# Netberry

## Execution
`py mainloop.py`

This will run the application on a loop, continuously monitoring the network.

## Description

This program runs until terminated by the user, continuously monitoring the network. Every five minutes, the program
will compile a list of all devices attached to the user's local network and compare them to the previous list, reporting
all new devices and all devices that have left the network since the last iteration. The results of the last run can
be accessed in `stdout`, by running `app/run_logreport.py`, or by viewing the `app/log.txt`


## Product Delivery Requirements

- All devices are listed for a user to view
- The monitoring tool, once started, works continuously until halted
- The user may view all devices that have connected to the network in the last five minutes
- The user may view all devices that have disconnected from the network in the last five minutes
- The user may view the host device's IP address to distinguish it from other conencted devices
- The user may view the last time the tool mapped all the connected network devices

## Methodology

This application is structured with a very clearly separated file structure. The `mapping` directory will house
the code used for the actual network map process, the `app` directory will hold the driver code for the map processes,
and the `root` directory holds the code to actually begint he process.

In `app/mapping/util.py`, the two methods that are essesntial to the program are stored. These two methods are used to
find the host device's full IP address, and the second method is used to ping an individual IP address to detect the
presence of a device on that address.

- `my_ip()` is useful not only to report the user's IP, but also to detect the
"neighborhood" in which to scan. While most local networks have this neighborhood be set to `192.168.0.XXX`, this is not
always true, so we use `my_ip()` to find this neighborhood.

- `ping(ip_target)` sends a `ping` command to the terminal with a target IP address and awaits a result. In order to
prevent the python console from becoming flooded with the output from `ping`, the output is piped to `devnull`, since it
cannot be easily suppressed. If `ping` returns no error, `ip_target` is returned, signalling success. If any errors are
encountered, we return `None`.

In `app/mapping/map.py`, we have one method, `map_local_network()`. This method utilizes both the util methods discussed
previously in order to generate a list of all occupied IP addresses on the network, effectively creating a list of all
network devices. We take the first 3 octets from `my_ip()` to generate the "neighborhood", then loop through all 256
possibilities for the fourth octet. The `ping` command can take quite some time to complete, especially if it does not
find a device, so we use the `multiprocessing` package to create several different `ping` processes simultaneously. This
allows us to `ping` several devices at once instead of waiting for the previous call to complete. The processes are
created in a `multiprocessing` `pool`, where we can easily push the output of the parallel processes into one list,
`good_ips`. In this implementation, we spin up 26 `processes`, allowing each to do around 10 calls to `utils.ping()`.
By using `multiprocessing`, we reduce the runtime of `map_local_network()` from 45 minutes to around 2 minutes.

In `app/run_map.py`, the `map_local_network()` function is called and results are recorded and reported. Results are
written to `app/log.txt` to be viewed next run. New and lost devices are reported, the runtime and date is recorded,
and quick device counts are produced.

In `app/run_logreport.py`, a quick report of the previous run is printed to the console, without actually running the
map process.

In `mainloop.py`, the `run_map.py` file is looped infinitely. The application should be started by running this file.
