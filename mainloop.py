from time import sleep

from app import run_map

# Activate the monitor
while True:
    run_map.main()
    sleep(300)  # run every five minutes - can be adjusted
